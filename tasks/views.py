from django.shortcuts import get_object_or_404, render, redirect
from.models import Tasks, Anggota
from .forms import formTasks, formAnggota
from django.http import HttpResponseRedirect
# Create your views here.

def index(request):
    return render(request, "tasks/index.html",{
        "task": Tasks.objects.all()
    })

def task(request, task_id):
    form = formAnggota(request.POST or None)
    if form.is_valid():
        form.save()
    task = Tasks.objects.get(pk=task_id)
    anggota = Anggota.objects.filter(task__id=task_id)
    return render(request, "tasks/task.html", {
        "task": task, "form":form, "anggota": anggota
    })

def create_anggota(request):
    form = formAnggota(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('../')
    context = {'form': form}
    return render(request, "tasks/create_anggota.html", context)

def create_task(request):
    form = formTasks(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('../')
    context = {'form': form}
    return render(request, "tasks/create_task.html", context)

def delete_task(request, task_id):
    obj=get_object_or_404(Tasks, pk=task_id)
    if request.method == "POST":
        obj.delete()
        return redirect('../../')
    context = {"object" : obj}
    return render(request, "tasks/delete_task.html", context)

