$( function() {
    $( "#accordion" )
      .accordion({
        header: "> div > h3"
      })
  } );

  $(document).ready(function(){
    var thisbox=0;
    var listofitems = $('#accordion');
    var len=$(listofitems).children().length;
  

  $("#accordion div").click(function(){
        thisbox= $(this).index();
    });

  $(".UP").click(function(e){
       e.preventDefault();
       if(thisbox>0)
       	{
       		jQuery($(listofitems).children().eq(thisbox-1)).before(jQuery($(listofitems).children().eq(thisbox)));
         	thisbox=thisbox-1;
     	}
  });

  $(".DOWN").click(function(e){
        e.preventDefault();
        if(thisbox < len)
        {
        	jQuery($(listofitems).children().eq(thisbox+1)).after(jQuery($(listofitems).children().eq(thisbox)));
          thisbox=thisbox+1;
     	}
    });

});