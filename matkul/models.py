from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator 

# Create your models here.
class Matkul(models.Model):
    name = models.CharField(max_length=64)
    dosen = models.CharField(max_length=64)
    sks = models.PositiveIntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(6)])
    desc = models.TextField()
    semester = models.CharField(max_length=9)
    ruang = models.CharField(max_length=20)

def __str__(self):
    return f"{self.name} : {self.dosen}, {self.sks} sks, {self.desc}, {self.semester}, {self.ruang}"