from django.shortcuts import render
from django.http import JsonResponse
import json
import requests


# Create your views here.

def index(request):
    response = {}
    return render(request, "book/index.html", response)

def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)

    if not request.user.is_authenticated:
        i = 0
        for x in data['items']:
            del data['items'][i]['volumeInfo']['imageLinks']['smallThumbnail']
            del data['items'][i]['volumeInfo']['publishedDate']
            i=i+1

    return JsonResponse(data, safe = False)