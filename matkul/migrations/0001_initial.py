# Generated by Django 3.1.2 on 2020-10-16 00:08

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Matkul',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
                ('dosen', models.CharField(max_length=64)),
                ('sks', models.IntegerField()),
                ('desc', models.CharField(max_length=100)),
                ('semester', models.CharField(max_length=9)),
                ('ruang', models.CharField(max_length=20)),
            ],
        ),
    ]
