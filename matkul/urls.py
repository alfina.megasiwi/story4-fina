from django.urls import path

from . import views

app_name = 'matkul'

urlpatterns = [
    path('', views.index, name='index'),
    path("<int:matkul_id>/", views.matkul, name="matkul"),
    path("create/", views.matkul_create, name="create_matkul"),
    path("<int:matkul_id>/delete/", views.matkul_delete, name="delete_matkul")
]
