from django.test import TestCase
from django.urls import reverse
from django.test import TestCase, Client
# Create your tests here.

class BookTestCase(TestCase):
    def test_index_url_exists(self):
        response = Client().get(reverse('book:index'))
        self.assertEquals(response.status_code, 200)

    def test_template_used(self):
        response = Client().get(reverse('book:index'))
        self.assertTemplateUsed(response,'book/index.html')
        
    def test_element_complete(self):   
        response = Client().get(reverse('book:index'))
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Welcome!", html_kembalian)
        self.assertIn("cari buku di sini yuk!", html_kembalian)
        self.assertIn("Title", html_kembalian)
        self.assertIn("Image", html_kembalian)
        self.assertIn("Date", html_kembalian)
        self.assertIn("Cari Buku", html_kembalian)
        
    