from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')
def photo(request):
    return render(request, 'main/photo.html')
def design(request):
    return render(request, 'main/design.html')
def story1(request):
    return render(request, 'main/story1.html')
    

