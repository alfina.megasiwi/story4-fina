from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from account.forms import RegistrationForms


def registration_view(request):
    context = {}
    if request.POST:
        form=RegistrationForms(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            account = authenticate(email=email, password= raw_password)
            login(request,account)
            return redirect('../book/')
        else:
            context['registration_form'] = form
    else:
        form=RegistrationForms()
        context['registration_form'] = form

    return render(request, 'account/register.html', context)