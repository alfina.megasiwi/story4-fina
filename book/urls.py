from django.urls import path

from . import views

app_name = 'book'

urlpatterns = [
    path('book/', views.index, name='index'),
    path('data/', views.fungsi_data, name='fungsi_data')

    
]
