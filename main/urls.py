from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name=''),
    path('home/', views.home, name='home'),
    path('photo/', views.photo, name='photo'),
    path('design/', views.design, name='design'),
    path('story1/', views.story1, name='story1')
]
