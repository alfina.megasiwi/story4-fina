from django.shortcuts import get_object_or_404, render, redirect
from.models import Matkul
from .forms import formMatkul

# Create your views here.

def index(request):
    return render(request, "matkul/index.html",{
        "matkul": Matkul.objects.all()
    })

def matkul(request, matkul_id):
    matkul = Matkul.objects.get(pk=matkul_id)
    return render(request, "matkul/matkul.html", {
        "matkul": matkul
    })

def matkul_create(request):
    form = formMatkul(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('../')
    context = {'form': form}
    return render(request, "matkul/matkul_create.html", context)

def matkul_delete(request, matkul_id):
    obj=get_object_or_404(Matkul, pk=matkul_id)
    if request.method == "POST":
        obj.delete()
        return redirect('../../')
    context = {"object" : obj}
    return render(request, "matkul/matkul_delete.html", context)

