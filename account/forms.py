from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import fields

from account.models import Account

class RegistrationForms(UserCreationForm):
    email = forms.EmailField(max_length=60, help_text='Required. Add a valid email address')

    class Meta:
        model = Account
        fields = ("email", "username", "password1", "password2")
        