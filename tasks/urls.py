from django.urls import path

from . import views

app_name = 'tasks'

urlpatterns = [
    path('', views.index, name='index'),
    path("<int:task_id>/", views.task, name="task"),
    path("create/", views.create_task, name="create_task"),
    path("<int:task_id>/delete/", views.delete_task, name="delete_task"),
    path("create_anggota/", views.create_anggota, name="create_anggota")
]
