from django.test import TestCase
from django.urls import reverse
from django.test import TestCase, Client
from account.models import Account
# Create your tests here.

class AccountTestCase(TestCase):
    def test_index_url_exists_accout(self):
        response = Client().get(reverse('account:register'))
        self.assertEquals(response.status_code, 200)

    def test_template_used_account(self):
        response = Client().get(reverse('account:register'))
        self.assertTemplateUsed(response,'account/register.html')
        
    def test_element_complete_account(self):   
        response = Client().get(reverse('account:register'))
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Register", html_kembalian)
        self.assertIn("Email:", html_kembalian)
        self.assertIn("Required. Add a valid email address", html_kembalian)
        self.assertIn("Username:", html_kembalian)
        self.assertIn("Password:", html_kembalian)
        self.assertIn("Your password can’t be too similar to your other personal information.", html_kembalian)
        self.assertIn("Your password must contain at least 8 characters.", html_kembalian)
        self.assertIn("Your password can’t be a commonly used password.", html_kembalian)
        self.assertIn("Your password can’t be entirely numeric.", html_kembalian)
        self.assertIn("Password confirmation", html_kembalian)
        self.assertIn("Enter the same password as before, for verification.", html_kembalian)
        self.assertIn("Register", html_kembalian)

    def test_model_account(self):
        Account.objects.create(email="aaa@gmail.com", username="aaa")
        total1 = Account.objects.all().count()
        self.assertEquals(total1, 1)
    