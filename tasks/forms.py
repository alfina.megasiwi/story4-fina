from django.db import models
from django import forms
from .models import Tasks, Anggota

class formTasks(forms.ModelForm):
    class Meta:
        model = Tasks
        fields = ['name']

class formAnggota(forms.ModelForm):
    class Meta:
        model = Anggota
        fields = ['name', 'task']