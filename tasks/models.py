from django.db import models

# Create your models here.
class Tasks(models.Model):
    name = models.CharField(max_length=64)
    def __str__(self):
        return self.name

class Anggota(models.Model):
    name = models.CharField(max_length=64)
    task = models.ForeignKey(Tasks, related_name='task', on_delete=models.CASCADE, default=1)
    def __str__(self):
        return self.name
 