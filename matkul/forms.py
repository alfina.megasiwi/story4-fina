from django.db import models
from django import forms
from .models import Matkul

class formMatkul(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ['name', 'dosen', 'sks', 'desc', 'semester', 'ruang']