
from django.http import response
from django.urls import reverse
from tasks.models import Anggota, Tasks
from django.test import TestCase, Client

# Create your tests here.
class TasksTestCase(TestCase):
    def test_index(self):
        response = Client().get(reverse('tasks:index'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'tasks/index.html')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Welcome!", html_kembalian)
        self.assertIn("daftarkan dirimu di sini", html_kembalian)
        self.assertIn("Add Tasks", html_kembalian)
        self.assertIn("Add Anggota", html_kembalian)

    def test_create_task(self):
        response = Client().get(reverse('tasks:create_task'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'tasks/create_task.html')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Add Tasks", html_kembalian)
        self.assertIn("Name:", html_kembalian)
        self.assertIn("Save", html_kembalian)
        self.assertIn("Back to the list", html_kembalian)
    
    def test_create_anggota(self):
        response = Client().get(reverse('tasks:create_anggota'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'tasks/create_anggota.html')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Add Anggota", html_kembalian)
        self.assertIn("Name:", html_kembalian)
        self.assertIn("Task:", html_kembalian)
        self.assertIn("Save", html_kembalian)
        self.assertIn("Back to the list", html_kembalian)

    def test_delete_task(self):
        tasks = Tasks.objects.create(name="aaa")
        response = Client().get(reverse('tasks:delete_task',args=[tasks.id]))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'tasks/delete_task.html')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Yes, Please :)", html_kembalian)
        self.assertIn("Nahhhhh :'(", html_kembalian)

    def test_task(self):
        tasks = Tasks.objects.create(name="aaa")
        response = Client().get(reverse('tasks:task',args=[tasks.id]))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'tasks/task.html')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Back to the list", html_kembalian)

    def test_model(self):
        tasks = Tasks.objects.create(name="aaa")
        total1 = Tasks.objects.all().count()
        self.assertEquals(total1, 1)

        Anggota.objects.create(name="ddd", task=tasks)
        total2 = Anggota.objects.all().count()
        self.assertEquals(total2, 1)

