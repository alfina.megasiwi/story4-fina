from django.test import TestCase
from django.urls import reverse
from django.test import TestCase, Client
# Create your tests here.

class AccordionTestCase(TestCase):
    def test_index_url_exists(self):
        response = Client().get(reverse('accordion:index'))
        self.assertEquals(response.status_code, 200)

    def test_template_used(self):
        response = Client().get(reverse('accordion:index'))
        self.assertTemplateUsed(response,'accordion/index.html')
        
    def test_element_complete(self):   
        response = Client().get(reverse('accordion:index'))
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Woooowww!", html_kembalian)
        self.assertIn("Accordion with JQuery? Freakin Cool 0-0", html_kembalian)
        self.assertIn("UP", html_kembalian)
        self.assertIn("DN", html_kembalian)
        self.assertIn("Akivitas", html_kembalian)
        self.assertIn("Pengalaman", html_kembalian)
        self.assertIn("Prestasi", html_kembalian)
        self.assertIn("Keluh Kesah", html_kembalian)